# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table address (
  id                            bigint auto_increment not null,
  street                        varchar(255),
  postal_code                   integer,
  city                          varchar(255),
  constraint pk_address primary key (id)
);

create table ingredient (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  constraint pk_ingredient primary key (id)
);

create table recipe (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  difficulty                    varchar(255),
  people                        integer,
  category                      varchar(255),
  user_id                       bigint,
  constraint pk_recipe primary key (id)
);

create table recipe_ingredient (
  recipe_id                     bigint not null,
  ingredient_id                 bigint not null,
  constraint pk_recipe_ingredient primary key (recipe_id,ingredient_id)
);

create table review (
  id                            bigint auto_increment not null,
  autor                         varchar(255),
  tittle                        varchar(255),
  content                       varchar(255),
  recipe_id                     bigint,
  constraint pk_review primary key (id)
);

create table user (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  last_name                     varchar(255),
  age                           integer,
  email                         varchar(255),
  address_id                    bigint,
  constraint uq_user_address_id unique (address_id),
  constraint pk_user primary key (id)
);

alter table recipe add constraint fk_recipe_user_id foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_recipe_user_id on recipe (user_id);

alter table recipe_ingredient add constraint fk_recipe_ingredient_recipe foreign key (recipe_id) references recipe (id) on delete restrict on update restrict;
create index ix_recipe_ingredient_recipe on recipe_ingredient (recipe_id);

alter table recipe_ingredient add constraint fk_recipe_ingredient_ingredient foreign key (ingredient_id) references ingredient (id) on delete restrict on update restrict;
create index ix_recipe_ingredient_ingredient on recipe_ingredient (ingredient_id);

alter table review add constraint fk_review_recipe_id foreign key (recipe_id) references recipe (id) on delete restrict on update restrict;
create index ix_review_recipe_id on review (recipe_id);

alter table user add constraint fk_user_address_id foreign key (address_id) references address (id) on delete restrict on update restrict;


# --- !Downs

alter table recipe drop constraint if exists fk_recipe_user_id;
drop index if exists ix_recipe_user_id;

alter table recipe_ingredient drop constraint if exists fk_recipe_ingredient_recipe;
drop index if exists ix_recipe_ingredient_recipe;

alter table recipe_ingredient drop constraint if exists fk_recipe_ingredient_ingredient;
drop index if exists ix_recipe_ingredient_ingredient;

alter table review drop constraint if exists fk_review_recipe_id;
drop index if exists ix_review_recipe_id;

alter table user drop constraint if exists fk_user_address_id;

drop table if exists address;

drop table if exists ingredient;

drop table if exists recipe;

drop table if exists recipe_ingredient;

drop table if exists review;

drop table if exists user;


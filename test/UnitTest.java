import akka.actor.ActorSystem;
import controllers.AsyncController;
import controllers.CountController;
import org.junit.Test;

import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import scala.concurrent.ExecutionContextExecutor;


import java.util.concurrent.CompletionStage;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static play.test.Helpers.contentAsString;

/**
 * Unit testing does not require Play application start up.
 *
 * https://www.playframework.com/documentation/latest/JavaTest
 */
public class UnitTest {



	@Test
	public void isValidEmailAddress() {
		
		String email = "pepe@gmail.com";
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        
        assertThat(m.matches()).isTrue();
 }
	
	@Test
	public void isInvalidEmailAddress() {
		
		String email = "pepe.com";
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        
        assertThat(m.matches()).isFalse();
 }
	
	@Test
	public void isValidCP() {
		
		String cp = "37185";
		
		String ePattern =  "0[1-9][0-9]{3}|[1-4][0-9]{4}|5[0-2][0-9]{3}";		
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(cp);
        
        assertThat(m.matches()).isTrue();
				
	}

	@Test
	public void isInvalidCP() {
		
		String cp = "371851";
		
		String ePattern =  "0[1-9][0-9]{3}|[1-4][0-9]{4}|5[0-2][0-9]{3}";		
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(cp);
   
        assertThat(m.matches()).isFalse();
				
	}
	
	
}

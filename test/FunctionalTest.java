
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import models.Address;
import models.Recipe;
import models.Review;
import models.User;

import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A functional test starts a Play application for every test.
 *
 * https://www.playframework.com/documentation/latest/JavaFunctionalTest
 */
public class FunctionalTest extends WithApplication {

	private Recipe recipe, recipe2;
	private User user1, user2;
	private Address address;
	ObjectNode recipeJson, recipeJson2, userJson1, userJson2;

	@Override
	protected Application provideApplication() {
		return Helpers.fakeApplication(Helpers.inMemoryDatabase());
	}

	@Before
	public void setValues() {

		this.address = getAddress2();
		this.address.save();
		
		this.user1 = getUser1();
		this.user2 = getUser2();
		
		this.recipe = getRecipe();
		this.recipe2 = getRecipe2();

		user2.save();
		recipe.save();

		recipeJson = (ObjectNode) Json.toJson(this.recipe);
		recipeJson2 = (ObjectNode) Json.toJson(this.recipe2);
		
		userJson1 = (ObjectNode) Json.toJson(this.user1);
		userJson2 = (ObjectNode) Json.toJson(this.user2);

	}

	private User getUser2() {
		
		User u = new User();
		u.setAge(12);
		u.setEmail("mail@usuario.es");
		u.setLastName("apellidos");
		u.setName("usuario");
		
		u.setAddres(this.address);
		return u;
	
	}
	private User getUser1() {
		
		User u = new User();
		u.setAge(12);
		u.setEmail("mail@usuario.es");
		u.setLastName("apellidos");
		u.setName("usuario");
	
		
		u.setAddres(getAddress());
		return u;
	}

	private Address getAddress() {
		Address address = new Address();
		address.setId((long)this.address.getId()+1);
		address.setCity("Springfield");
		address.setPostalCode(12345);
		address.setStreet("742 de Evergreen Terrace");
		
		address.setUser(this.user1);
		

		return address;
	}
	
	private Address getAddress2() {
		
		Address address = new Address();
		address.setCity("Springfield");
		address.setPostalCode(12345);
		address.setStreet("742 de Evergreen Terrace");
		
		return address;
	}

	private Recipe getRecipe() {

		Recipe recipe = new Recipe();
		recipe.setName("MenestraDeVerduras");
		recipe.setCategory("carne");
		recipe.setDifficulty("medium");
		recipe.setPeople(4);

		recipe.addReview(new Review("Juan", "titulo", "contenido", recipe));

		return recipe;
	}

	private Recipe getRecipe2() {

		Recipe recipe = new Recipe();
		recipe.setId(31);
		recipe.setName("MenestraDeVerduras");
		recipe.setCategory("carne");
		recipe.setDifficulty("medium");
		recipe.setPeople(4);
		recipe.addReview(new Review("Juan", "titulo", "contenido", recipe));

		return recipe;
	}
	
	
	
	
	@Test
	public void TestAddUserOk() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("POST").uri("/user")
				.header("Content-Type", "application/json").bodyJson(userJson1);

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(201);

	}
	
	@Test
	public void TestAddUserErrorRoute() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("POST").uri("/user/")
				.header("Content-Type", "application/json").bodyJson(userJson1);

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(404);

	}
	
	@Test
	public void TestGetUserById() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("GET").uri("/user" + "/id/" + this.user2.getId())
				.header("Accept", "application/json");

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(200);
		assertThat(r.contentType().orElse("")).isEqualTo("application/json");

	}
	
	@Test
	public void TestGetUserByIdErrornotFound() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("GET").uri("/user" + "/id/" + this.user2.getId()+1)
				.header("Accept", "application/json");

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(404);
	

	}
	
	@Test
	public void TestGetUserByIdXML() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("GET").uri("/user" + "/id/" + this.user2.getId())
				.header("Accept", "application/xml");

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(200);
		assertThat(r.contentType().orElse("")).isEqualTo("application/xml");

	}
	
	
	@Test
	public void TestRemoveUser() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("DELETE").uri("/user/id/" + this.user2.getId())
				.header("Accept", "application/json");

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(200);

	}
	
	@Test
	public void TestRemoveUserErrorNotFound() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("DELETE").uri("/user/id/" + this.user2.getId()+1)
				.header("Accept", "application/json");

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(404);

	}
	
	@Test
	public void TestAddRecipeOk() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("POST").uri("/recipe/"+this.user2.getId())
				.header("Content-Type", "application/json").bodyJson(recipeJson2);

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(201);

	}
	
	@Test
	public void TestAddRecipeErrorRoute() {


		Http.RequestBuilder req = Helpers.fakeRequest().method("POST").uri("/recipe/")
				.header("Content-Type", "application/json").bodyJson(recipeJson);

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(404);

	}

	@Test
	public void TestRemoveRecipe() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("DELETE").uri("/recipe/id/" + this.recipe.getId())
				.header("Accept", "application/json");

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(200);

	}
	
	@Test
	public void TestRemoveRecipeErrorNotFound() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("DELETE").uri("/recipe/id/" + this.recipe.getId()+1)
				.header("Accept", "application/json");

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(404);

	}

	@Test
	public void TestGetRecipeById() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("GET").uri("/recipe" + "/id/" + recipe.getId())
				.header("Accept", "application/json");

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(200);
		assertThat(r.contentType().orElse("")).isEqualTo("application/json");

	}
	
	@Test
	public void TestGetRecipeByIdXml() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("GET").uri("/recipe" + "/id/" + recipe.getId())
				.header("Accept", "application/xml");

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(200);
		assertThat(r.contentType().orElse("")).isEqualTo("application/xml");

	}
	
	@Test
	public void TestGetRecipeByIdErrorNotFound() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("GET").uri("/recipe" + "/id/" + recipe.getId()+1)
				.header("Accept", "application/json");

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(404);
	

	}

	@Test
	public void TestGetRecipeByName() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("GET").uri("/recipe/name/" + recipe.getName())
				.header("Accept", "application/json");

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(200);
		assertThat(r.contentType().orElse("")).isEqualTo("application/json");

	}
	
	/*
	@Test
	public void TestGetRecipeByNameXml() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("GET").uri("/recipe/name/" + recipe.getName())
				.header("Accept", "application/xml");

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(200);
		assertThat(r.contentType().orElse("")).isEqualTo("application/xml");

	}*/
	
	@Test
	public void TestGetRecipeByNameErrorNotFound() {

		Http.RequestBuilder req = Helpers.fakeRequest().method("GET").uri("/recipe/name/" + "estaRecetaNoEiste")
				.header("Accept", "application/json");

		Result r = Helpers.route(app, req);

		assertThat(r.status()).isEqualTo(404);
		

	}
	
	

}

package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;


import com.fasterxml.jackson.annotation.JsonIgnore;
import io.ebean.Model;
import play.data.validation.Constraints.Required;

@Entity
public class Address extends Model {
	
	
	@Id
	private Long id;
	
	@Required(message="calle-requerida")
	private String street;
	
	@Required(message="cp-requerido")
	private Integer postalCode;
	
	@Required(message="ciudad-requerido")
	private String city;
	
	@JsonIgnore
	@OneToOne (mappedBy="address")
	public User user;
	
	
	public Address( String street, Integer postalCode, String city, User user) {
	
		this.street = street;
		this.postalCode = postalCode;
		this.city = city;
		this.user = user;
	}

	public Address() {
		
	}	
	
	
	
	public long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public Integer getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(Integer postalCode) {
		this.postalCode = postalCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}

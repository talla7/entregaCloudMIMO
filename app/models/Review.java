package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.annotation.JsonIgnore;


import io.ebean.Model;

@Entity
public class Review extends Model {

	
	
	@Id
	private long id;

	private String autor;
	private String tittle;
	private String content;


	
	@ManyToOne @JsonIgnore
	private Recipe recipe;

	public Review(String autor, String tittle, String content, Recipe recipe) {
		
		this.autor = autor;
		this.tittle = tittle;
		this.content = content;
		this.recipe = recipe;
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTittle() {
		return tittle;
	}

	public void setTittle(String tittle) {
		this.tittle = tittle;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}

	
}

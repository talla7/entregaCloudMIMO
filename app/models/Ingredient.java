package models;



import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.ebean.Finder;
import io.ebean.Model;


@Entity
public class Ingredient extends Model {
	
	public static final Finder<Long,Ingredient> find =  new Finder<>(Ingredient.class);
	
	@Id
	private long id;
	private String name;
	
	@JsonIgnore
	@ManyToMany(mappedBy="ingredients")
	public List<Recipe> recipes = new ArrayList<>();
	
	public Ingredient () {
		
		
	}
	
	
	@JsonIgnore
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}

	public List<Recipe> getRecipes() {
		return recipes;
	}


	public void setRecipes(List<Recipe> recipes) {
		this.recipes = recipes;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		
		this.name = name;
	}


	public static Ingredient findByName(String name) {
		 
		 if (name == null) {
			 throw new IllegalArgumentException();
		 }
		 
		 return find.query().where().eq("name", name).findOne();
		 
	 }


	

}

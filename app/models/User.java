package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.databind.JsonNode;

import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Required;
import play.libs.Json;

@Entity
public class User extends Model{

	public static final Finder<Long,User> find =  new Finder<>(User.class);
	
	@Id
	private Long id;
	
	@Required(message="nombre-requerido")
	private String name;
	
	private String lastName;
	
	private Integer age;
	
	@Required(message="email-requerido")
	@Email(message="email-no-valido")
	private String email;
	

	@Required(message="direccion-requerida")
	@OneToOne(cascade=CascadeType.ALL) 
	public Address address;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="user") 
	private List<Recipe> recipes = new ArrayList<>();

	
	
	public User(Long id, String name, String lastName, Integer age, String email,
			List<Recipe> recipes, Address address) {
		this.name = name;
		this.lastName = lastName;
		this.age = age;
		this.email = email;
		this.recipes = recipes;
		this.address = address;
	}
	
	public User() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Recipe> getRecipes() {
		return recipes;
	}

	public void setRecipes(List<Recipe> recipes) {
		this.recipes = recipes;
	}
	

	public Address getAddres() {
		return address;
	}

	public void setAddres(Address addres) {
		this.address = addres;
	}

	public static User findById(Integer id) {

		 if (id == null) {
			 throw new IllegalArgumentException();
		 }
		 
		 return find.query().where().eq("id", id).findOne();
    }
    
    public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public static List<User> findByName(String name) {

        return find.query().where().eq("name", name).findList();
    }
    
    public static List<User> findByAge(Integer age) {

        return find.query().where().eq("age", age).findList();
    }
    
    
	public static List<User> searchAll() { 
		 return find.query()
				 .findList();
		 
	 }
	
	public JsonNode toJson() {
		
		return Json.toJson(this);
	}
	
}

package models;

import io.ebean.Model;
import play.data.validation.Constraints.Required;

import play.libs.Json;

import java.util.*;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.JsonNode;


import io.ebean.Finder;



@Entity
public class Recipe extends Model{
	
	public static final Finder<Long,Recipe> find =  new Finder<>(Recipe.class);

	@Id
	private Long id;
	
	@Required(message="nombre-requerido")
	private String name;
	
	private String difficulty;
	private Integer people;
	
	
	@Required(message="categoria-requerida")
	private String category;
	
	//@Min(2)

	@ManyToMany(cascade=CascadeType.ALL) 
	private List<Ingredient> ingredients = new ArrayList<Ingredient>();
	
	
	@OneToMany(cascade=CascadeType.ALL) 
	private List<Review> reviews = new ArrayList<>();

    @JsonIgnore
    @ManyToOne
    private User user;

	public Recipe (String name, String difficulty, Integer people, String category, List <Ingredient> ingredients,List <Review> reviews){
			
		this.name = name;
		this.difficulty = difficulty;
		this.people = people;
		this.category = category;
		this.ingredients = ingredients;
		this.reviews = reviews;

	}
		
	public Recipe(){
		super();	
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public Integer getPeople() {
		return people;
	}

	public void setPeople(Integer people) {
		this.people = people;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	public List<Ingredient> getIngredients() {
		return this.ingredients;
	}
	
	public List<Review> getReviews() {
		return reviews;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public void addIngredient (String name, int i) {
		
		Ingredient ingredient = Ingredient.findByName(name);
		
		if (ingredient==null) {
			ingredient = new Ingredient();
			ingredient.setName(name);
			ingredient.save();
			System.out.println("creo el ingrediente nuevo");
			
		}
		
		ingredient.getRecipes().add(this);		
		this.ingredients.add(ingredient);
		
	}
	
	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
		
	public void addReview (Review re) {
		this.reviews.add(re);
	}
	
	
	public void updateIngredients (JsonNode ingredients) {
		
		List<Ingredient> newIngredients = new ArrayList<Ingredient>();
		
		for(JsonNode i : ingredients) {
			
			//Ingredient ingredient =  new Ingredient(i.get("name").asText(), this);
			//newIngredients.add(ingredient);
		}
		
		this.setIngredients(newIngredients);
		
	}
 
	public static List<Recipe> search(String param, String name) {//encuentra todas las recetas que tengan 
		 return find.query()
				 	.where()
				 		.contains(param, name)
				 		.findList();
		 
	 }
	
	public static List<Recipe> searchAll() { 
		 return find.query()
				 .findList();
		 
	 }
	
	public static Recipe findByName(String name) {
		 
		 if (name == null) {
			 throw new IllegalArgumentException();
		 }
		 
		 return find.query().where().eq("name", name).findOne();
		 
	 }
	
	public static Recipe findById(Integer id) {
		 
		 if (id == null) {
			 throw new IllegalArgumentException();
		 }
		 
		 return find.query().where().eq("id", id).findOne();
		 
	 }

	public JsonNode toJson() {
		
		return Json.toJson(this);
	}
	
	public boolean isDuplicated (Integer id) {
		
		Recipe r = findById(id);
		
		if (r!=null) {
			return true;
		}
		else {return false;}
	}
	
	
	
}

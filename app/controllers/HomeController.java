package controllers;


import play.i18n.Messages;

import play.mvc.*;

import views.html.*;

public class HomeController extends Controller {

	public String escribirMensaje(String mensaje) {
		Messages messages = Http.Context.current().messages();
		return messages.at(mensaje);
	}

	public Result index() {
		return ok(index.render(escribirMensaje("aplicacion-preparada")));
	}
	
	
	

}

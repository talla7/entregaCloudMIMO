package controllers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.ebean.Finder;
import io.ebean.annotation.Transactional;
import models.Ingredient;
import models.Recipe;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.*;
import play.cache.SyncCacheApi;

import views.html.*;


public class RecipeController extends Controller {

	@Inject
	FormFactory formFactory;

	@Inject
	SyncCacheApi cache;

	private List<Recipe> recipes = new ArrayList<>();

	public String escribirMensaje(String mensaje) {
		Messages messages = Http.Context.current().messages();
		return messages.at(mensaje);
	}

	@Transactional
	public Result añadirRecipe(Integer idUser) {

		Form<Recipe> recipeForm = formFactory.form(Recipe.class).bindFromRequest();

		if (recipeForm.hasErrors()) {

			return ok(recipeForm.errorsAsJson());
		}

		Recipe receta = recipeForm.get();
		
		List<Ingredient> ingredientes = new ArrayList<Ingredient>(receta.getIngredients());
			
		receta.getIngredients().clear();
			
		for(int i=0;i<ingredientes.size();i++) {		
			receta.addIngredient(ingredientes.get(i).getName(), i);		
		}

		
		User user = (User) User.findById(idUser);
		
		if(user==null) {
			
			return Results.notFound(escribirMensaje("El-usuario-asociado-a-la-receta-no-existe"));
			
		}else {
			
			receta.setUser(user);
			
			receta.save();
	
			return Results.created(escribirMensaje("receta-creada"));
		}

	}

	public Result getRecipeByName(String nameRecipe) {

		List<Recipe> r = Recipe.search("name", nameRecipe);
		if (!r.isEmpty()) {
			if (request().accepts("application/json")) {
				return ok(Json.toJson(r));
			}else if (request().accepts("application/xml")) {
				return ok(views.xml.recipes.render(r));
			} 

		} else {
			return Results.notFound(escribirMensaje("receta-no-encontrada"));
		}
		return Results.notFound(escribirMensaje("receta-no-encontrada"));

	}

	public Result getRecipeByCategory(String categoryRecipe) {

		List<Recipe> r = Recipe.search("category", categoryRecipe);
		if (!r.isEmpty()) {
			if (request().accepts("application/json"))
				return ok(Json.toJson(r));
			else if(request().accepts("application/xml"))
				return ok(views.xml.recipes.render(r));
			else
				return Results.notFound(escribirMensaje("receta-no-encontrada"));
		} else {
			return Results.notFound(escribirMensaje("receta-no-encontrada"));
		}
	}

	public Result getRecipeByDifficulty(String difficultyRecipe) {

		List<Recipe> r = Recipe.search("difficulty", difficultyRecipe);
		if (!r.isEmpty()) {
			if (request().accepts("application/json"))
				return ok(Json.toJson(r));
			else if(request().accepts("application/xml"))
				return ok(views.xml.recipes.render(r));
			else
				return Results.notFound(escribirMensaje("receta-no-encontrada"));
		} else {
			return Results.notFound(escribirMensaje("receta-no-encontrada"));
		}
	}

	public Result getAllRecipes() {
		List<Recipe> r = Recipe.searchAll();
		if (!r.isEmpty()) {
			if (request().accepts("application/json"))
				return ok(Json.toJson(r));
			else if(request().accepts("application/xml"))
				return ok(views.xml.recipes.render(r));
			else
				return Results.notFound(escribirMensaje("receta-no-encontrada"));		
		} else {
			return Results.notFound(escribirMensaje("receta-no-encontrada"));
		}
	}

	public Result getRecipe(Integer idRecipe) {

		Recipe r = cache.get(getCacheKey(idRecipe));
		if (r == null) {
			r = Recipe.findById(idRecipe);
			cache.set(getCacheKey(idRecipe), r);

		}

		if (r != null) {

			if (request().accepts("application/xml")) {

				return ok(views.xml.recipe.render(r));

			} else if (request().accepts("application/json")) {

				return ok(r.toJson());

			} else {

				return status(415);
			}

		} else {

			return Results.notFound(escribirMensaje("receta-no-encontrada"));
		}

	}

	public Result deleteRecipe(Integer idRecipe) {

		Recipe r = Recipe.findById(idRecipe);

		if (r == null) {
			return Results.notFound(escribirMensaje("receta-no-encontrada"));
		}

		if (r.delete()) {
			cache.remove(getCacheKey(idRecipe));
			return ok(escribirMensaje("receta-eliminada"));
		} else {
			return internalServerError();
		}

	}

	public Result updateRecipe(Integer idRecipe) {

		Boolean isUpdated = false;

		Recipe r = Recipe.findById(idRecipe);

		if (r == null) {
			return Results.notFound(escribirMensaje("receta-no-encontrada"));
		} else {

			JsonNode body = request().body().asJson();
			if (body.has("name")) {
				r.setName(body.get("name").asText());
				isUpdated = true;
			}
			if (body.has("difficulty")) {
				r.setDifficulty(body.get("difficulty").asText());
				isUpdated = true;

			}
			if (body.has("people")) {
				r.setPeople(body.get("people").asInt());
				isUpdated = true;
			}
			if (body.has("category")) {
				r.setCategory(body.get("category").asText());
				isUpdated = true;
			}
			if (body.has("ingredients")) {

				r.updateIngredients(body.get("ingredients"));
				isUpdated = true;

			}
			if (isUpdated) {
				r.update();
				cache.remove(getCacheKey(idRecipe));
				return Results
						.ok(escribirMensaje(escribirMensaje("receta")) + r.getName() + escribirMensaje("actualizada"));
			}
			return Results.ok(escribirMensaje("receta-no-actualizada"));

		}

	}
	
	public String getCacheKey (long idRecipe) {
			
			return "recipe_"+idRecipe;
		}

}

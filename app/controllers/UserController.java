package controllers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.ebean.Finder;
import io.ebean.annotation.Transactional;
import models.Ingredient;
import models.Recipe;
import models.User;
import models.Recipe.Category;
import models.Recipe.Difficulty;
import play.data.Form;
import play.data.FormFactory;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.*;
import play.cache.SyncCacheApi;

import views.html.*;


public class UserController extends Controller {

	@Inject
	FormFactory formFactory;

	@Inject
	SyncCacheApi cache;

	private List<User> users = new ArrayList<>();

	public String escribirMensaje(String mensaje) {
		Messages messages = Http.Context.current().messages();
		return messages.at(mensaje);
	}

	
	@Transactional
	public Result añadirUser() {

		Form<User> userForm = formFactory.form(User.class).bindFromRequest();

		if (userForm.hasErrors()) {
			return ok(userForm.errorsAsJson());
		}
		User user = userForm.get();

		user.save();

		return Results.created(escribirMensaje("usuario-creado"));
	}
	
	


	public Result getUser(Integer idUser) {
		
		User u = cache.get(getCacheKey(idUser));
		
		if(u==null) {
			
			u = (User) User.findById(idUser);
			cache.set(getCacheKey(idUser), u);
			
		}
		
		System.out.println("tralariiii");
		if (u != null) {

			if (request().accepts("application/xml")) {
				return ok(views.xml.user.render(u));

			} else if (request().accepts("application/json")) {

				return ok(u.toJson());
			} else {
				return status(415);
			}

		} else {

			return Results.notFound(escribirMensaje("usuario-no-encontrado"));
		}

	}

	public Result deleteUser(Integer idUser) {

		User u = User.findById(idUser);

		if (u == null) {
			return Results.notFound(escribirMensaje("usuario-no-encontrado"));
		}

		if (u.delete()) {
			cache.remove(getCacheKey(idUser));
			return ok(escribirMensaje("usuario-eliminada"));
		} else {
			return internalServerError();
		}

	}

	public Result getAllUsers() {
		List<User> u = User.searchAll();
		if (!u.isEmpty()) {
			if (request().accepts("application/json"))
				return ok(Json.toJson(u));
			else if(request().accepts("application/xml"))
				return ok(views.xml.users.render(u));
			else
				return Results.notFound(escribirMensaje("usuario-no-encontrado"));
		} else {
			return Results.notFound(escribirMensaje("usuario-no-encontrado"));
		}
	}

	public Result getUserByName(String nameUser) {

		
		List<User> us = User.findByName(nameUser);

		if (!us.isEmpty()) {
				if (request().accepts("application/json"))
					return ok(Json.toJson(us));
				else if(request().accepts("application/xml"))
					return ok(views.xml.users.render(us));
				else
					return Results.notFound(escribirMensaje("usuario-no-encontrado"));
		} else {
			return status(415);
		}
	}

	public Result getUserByAge(Integer ageUser) {

		List<User> us = User.findByAge(ageUser);
		if (!us.isEmpty()) {
			if (request().accepts("application/json"))
				return ok(Json.toJson(us));
			else if(request().accepts("application/xml"))
				return ok(views.xml.users.render(us));
			else
				return Results.notFound(escribirMensaje("usuario-no-encontrado"));
		} else {
			return status(415);
		}
	}


	public Result updateUser(Integer idUser) {

		Boolean isUpdated = false;

		User u = User.findById(idUser);

		if (u == null) {
			return Results.notFound(escribirMensaje("usuario-no-encontrado"));
		} else {

			JsonNode body = request().body().asJson();
			if (body.has("name")) {
				u.setName(body.get("name").asText());
				isUpdated = true;
			}
			if (body.has("lastName")) {
				u.setLastName(body.get("lastName").asText());
				isUpdated = true;

			}
			if (body.has("age")) {
				u.setAge(body.get("age").asInt());
				isUpdated = true;
			}
			if (body.has("email")) {
				u.setEmail(body.get("email").asText());
				isUpdated = true;
			}

			if (isUpdated) {
				u.update();
				cache.remove(getCacheKey(idUser));
				return Results
						.ok(escribirMensaje(escribirMensaje("usuario")) + u.getName() + escribirMensaje("actualizado"));
			}
			return Results.ok(escribirMensaje("usuario-no-actualizado"));

		}

	}
	
private String getCacheKey(Integer idUser) {
		
		return "user_"+idUser;	
	}

}
